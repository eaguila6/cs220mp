// imageManip.h
// 601.220, Fall 2018
// Claire Zou and Eduardo Aguila
// czou6 and eaguila6
#include <stdio.h>
#include "ppm_io.h"

#ifndef IMAGE_MANIP_H
#define IMAGE_MANIP_H

/* Parameter is an image pointer to the copy of the image to be altered   
 * Function swaps the color channels for each pixel (b->g, g->r, r->b)
 * Returns the image pointer of the altered image 
 */
Image * swap(Image *ip);

/* Parameter is an image pointer to the copy of the image to be altered 
 * Function turns the image into a grayscale using the NTSC standard conversion formula
 * Returns the image pointer of the altered image 
 */
Image * grayscale(Image *ip);

/* Helper function for contrast function 
 * Parameter is the value to be clamped 
 */
void clamp(double * value);

/* Parameters are an image pointer to the copy of the image to be altered 
 * and the double factor to which the image will be contrasted 
 * Function contrasts the image according to the factor 
 * Returns the image pointer of the altered image 
 */
Image * contrast(Image *ip, double factor);

/* Parameter is an image pointer to the copy of the image to be altered 
 * Functions zooms in the image, making it larger 
 * Returns the image pointer of the altered image 
 */
Image * zoom_in(Image *ip);

/* Parameter is an image pointer to the copy of the image to be altered 
 * Function zooms out the image, making it smaller 
 * Returns the image pointer of the altered image 
 */
Image * zoom_out(Image *ip);

/* Parameters are an image pointer to the copy of the image to be altered 
 * as well as the section of the image to be included, indicating by the 
 * pixel locations as ints 
 * Functions turns the color channels within the region into black pixels 
 * Returns the image pointer of the altered image 
 */
Image * occlude(Image *ip, int up_left_r, int up_left_c, int bot_right_r, int bot_right_c);

/* Helper function for the blur function 
 * Creates the matrix that will be used according to the sigma value 
 */
double * nxnmatrix(double sigma);

/* Parameters are an image pointer to the copy of the image to be altered as well 
 * as well as the sigma value to indicate the level of the blur 
 * Function blurs the image according to the specified value
 * Returns the image pointer of the altered image 
 */
Image * blur(Image *ip, double factor);

#endif
