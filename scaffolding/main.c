#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"
#include "imageManip.h"

int main(int argc, char* argv[]) {
  if(argc < 4) { //check for args values within each one? 
    printf("Please enter a valid command line prompt\n");
    return 5;
  }
  //add error conditions 
 
  FILE *fp = fopen(argv[1], "rb"); //open file for reading

  Image * copy = read_ppm(fp); //call read_ppm function on fp
  fclose(fp);
  
  if (strcmp(argv[3], "swap") == 0) {
    swap(copy);
  } else if (strcmp(argv[3], "grayscale") == 0) {
    grayscale(copy);
  } else if (strcmp(argv[3], "contrast") == 0) {
    contrast(copy, atof(argv[4]));
  } else if (strcmp(argv[3], "zoom_in") == 0) {
    zoom_in(copy);
  } else if (strcmp(argv[3], "zoom_out") == 0) {
    zoom_out(copy);
  } else if (strcmp(argv[3], "occlude") == 0) {
    occlude(copy, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
  }
  
  FILE *fp2 = fopen(argv[2], "wb");
  write_ppm(fp2, copy);
  fclose(fp2); 
}
