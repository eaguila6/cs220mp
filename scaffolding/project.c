// project.c
// 601.220, Fall 2018
// Claire Zou and Eduardo Aguila
// czou6 and eaguila6

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"
#include "imageManip.h"
#include "operationmenu.h"

/* Main function for the midterm project 
 * Takes in command line arguments from the user 
 * Returns int depending on success, 0 if successful and
 * non-zero for non-successul
 */
int main(int argc, char* argv[]) {
  operationmenu(argc, argv);
  return 0;
}
