// ppm_io.c
// 601.220, Fall 2018
// Claire Zou and Eduardo Aguila
// czou6 and eaguila6

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"


/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */

Image * read_ppm(FILE *fp, int row, int col) { //call nika.ppm from main
  assert(fp); //check that fp is not NULL
  
  Image *img; //creates pointer to new Image struct
  img = (Image *)malloc(sizeof(Image)); //malloc memory for img  
  img->data = (Pixel*)malloc(row * col * sizeof(Pixel)); //malloc memory for each pixel 
  img->rows = row;
  img->cols = col;
  fgetc(fp);
  fread(img->data, sizeof(Pixel), img->rows * img->cols, fp);
  fclose(fp);
  return img;
}

/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 **/
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->rows * im->cols, fp);

  if (num_pixels_written != im->rows * im->cols) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}
