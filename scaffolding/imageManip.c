// ppm_io.c
// 601.220, Fall 2018
// Claire Zou and Eduardo Aguila
// czou6 and eaguila6
#include <stdio.h>
#include <stdlib.h>
#include "imageManip.h"
#include <math.h>
#define PI 3.14159265358979323846

/* Function takes in an image pointer as its only parameter 
 * Loops through the entire image, acessing the data in each pixel
 * Creates temp variables to hold the values of green and blue for the pixel  
 * Reassigns color values as indicated by swap functions instructions (b->g, g->r, r->b)
 * Returns an image pointer to the altered image 
 */
Image * swap(Image *ip) {
  for(int i = 0; i < ip->rows * ip->cols; i++) {
    unsigned char gtemp = ip->data[i].g;
    unsigned char rtemp = ip->data[i].r;
    ip->data[i].g = ip->data[i].b;
    ip->data[i].r = gtemp;
    ip->data[i].b = rtemp;
  }
  return ip;
}

/* Function takes in an image pointer as its only parameter
 * Loops through the entire image, acessing the data in each pixel
 * Takes the values for r, b, and g at each pixel 
 * Converts the value to grayscale using the NTSC standard conversion formula
 * Assigns the calculated gray value back to each of the color channels 
 * Returns an image pointer to the altered image 
 */
Image * grayscale(Image *ip) {
  unsigned char red, blue, green;
  double gray;  
  for(int i = 0; i < ip->rows * ip->cols; i++) {
      red = ip->data[i].r;
      blue = ip->data[i].b;
      green = ip->data[i].g;
      gray = (.30 * red) + (.59 * green) + (.11 * blue);
      ip->data[i].r = (char)gray;
      ip->data[i].b = (char)gray;
      ip->data[i].g = (char)gray;
  }
  return ip;
}

/* Helper function used in the contrast function defined below 
 * Takes in a double pointer as its only parameter 
 * If the value is greater/less than the highest/lowest possible RBG value, it is clamped 
 * void function, value is changed as a pointer and accessible in contrast function 
 */
void clamp(double * value) {
  if(*value > 255){
    *value = 255;
  }
  if(*value < 0) {
    *value = 0;
  }
}

/* Function takes in an image pointer and contrast value as the two parameters  
 * Then creates a double and double pointer, to be used to store the color 
 * channel value and then send to the clamp function 
 * Repeats for each color channel
 * Returns an image pointer to the altered image 
 */
Image * contrast(Image *ip, double factor) {
  double val;
  double * val1;
  
  for(int i = 0; i < ip->rows * ip->cols; i++){
    val = ((((ip->data[i].r / 255.0) - 0.5) * factor) + .5) * 255;
    val1 = &val;
    clamp(val1);
    ip->data[i].r = (unsigned char)val;

    val = ((((ip->data[i].b / 255.0) - 0.5) * factor) + .5) * 255;
    val1 = &val;
    clamp(val1);
    ip->data[i].b = (unsigned char)val;

    val = ((((ip->data[i].g / 255.0) - 0.5) * factor) + .5) * 255 ;
    val1 = &val;
    clamp(val1);
    ip->data[i].g = (unsigned char)val;
  }
  return ip; 
}

/* Function takes in an image pointer as its only parameter  
 * Then creates a new image pointer with dimensions that are
 * twice as large as the original image 
 * Loops through each pixel in every other row (starting with row 0) in the new image
 * Copies each pixel from the old image into the new image
 * Loops through each pixel in every other row (starting with row 1) in the new image
 * Copies each pixel from the old image into the new image
 * Changes dimension values in original image pointer and 
 * dyanmically reallocates data to be twice as large
 * Loops through each pixel in the old image now with its new dimensions 
 * Copies each pixel from new image pointer to old image pointer  
 * Returns an image pointer to the altered image
 */
Image * zoom_in(Image *ip) {
  Image *big_img = (Image *)malloc(sizeof(Image));
  big_img->rows = ip->rows * 2;
  big_img->cols = ip->cols * 2;
  big_img->data = (Pixel *)malloc(big_img->rows * big_img->cols * sizeof(Pixel));

  for (int i = 0, x = 0; i < ip ->rows; i++, x++) {
    for (int j = 0, y = 0; j < ip->cols; j++, y++) {
      big_img->data[x * big_img->cols + y] = ip->data[i * ip->cols + j];
      big_img->data[x * big_img->cols + ++y] = ip->data[i * ip->cols + j];
    }
    x++;
  }

  for (int i = 0, x = 1; i < ip ->rows; i++, x++) {
    for (int j = 0, y = 0; j < ip->cols; j++, y++) {
      big_img->data[x * big_img->cols + y] = ip->data[i * ip->cols + j];
      big_img->data[x * big_img->cols + ++y] = ip->data[i * ip->cols + j];
    }
    x++;
  }

  ip->rows = big_img->rows;
  ip->cols = big_img->cols;
  ip->data = (Pixel *)realloc(ip->data, ip->rows * ip->cols * sizeof(Pixel));
  for(int i = 0; i < ip->rows * ip->cols; i++) {
    ip->data[i] = big_img->data[i];
  }

  free(big_img->data);
  free(big_img);
  return ip;
}

/* Function takes in an image pointer as its only parameter  
 * Then creates a new image pointer with dimensions that are
 * half the size of the original image 
 * Loops through each pixel in the new image
 * Takes the average of every color channel in every four pixel square 
 * and assigns it to each pixel in the new iamge  
 * Changes dimension values in original image pointer and 
 * dyanmically reallocates data to be half the size
 * Loops through each pixel in the old image now with its new dimensions 
 * Copies each pixel from new image pointer to old image pointer  
 * Returns an image pointer to the altered image
 */
Image * zoom_out(Image *ip) {
  Image * small_img = (Image *)malloc(sizeof(Image));
  small_img->rows = ip->rows / 2;
  int rows = small_img->rows; 
  small_img->cols = ip->cols / 2;
  int orig_c = ip->cols;
  int cols = small_img->cols;
  small_img->data = (Pixel *)malloc(rows * cols * sizeof(Pixel));

  for (int i = 0, x = 0; i < rows; i++, x++) {
    for (int j = 0, y = 0; j < cols; j++, y++) {
      small_img->data[i * cols + j].r = (ip->data[x * orig_c + y].r + ip->data[x * orig_c + y + 1].r
					 + ip->data[(x + 1) * orig_c + y].r
					 + ip->data[(x + 1) * orig_c + y + 1].r) / 4;
      small_img->data[i * cols + j].b = (ip->data[x * orig_c + y].b + ip->data[x * orig_c + y + 1].b
					 + ip->data[(x + 1) * orig_c + y].b
					 + ip->data[(x + 1) * orig_c + y + 1].b) / 4;
      small_img->data[i * cols + j].g = (ip->data[x * orig_c + y].g + ip->data[x * orig_c + y + 1].g
					 + ip->data[(x + 1) * orig_c + y].g
					 + ip->data[(x + 1) * orig_c + y + 1].g) / 4;
      y++;
    }
    x++;
  }

  ip->rows = rows;
  ip->cols = cols;
  ip->data = (Pixel *)realloc(ip->data, rows * cols * sizeof(Pixel));
  for(int i = 0; i < ip->rows * ip->cols; i++) {
    ip->data[i] = small_img->data[i];
  }

  free(small_img->data);
  free(small_img);
  return ip;
}

/* Function takes in image pointer, as well as 4 int parameters indicating the region
 * that will be occluded 
 * Function loops through each of the pixels within this region and assigns 0 to each
 * color channel within the pixel 
 * Returns an image pointer to the altered image 
 */
Image * occlude(Image *ip, int up_left_r, int up_left_c, int bot_right_r, int bot_right_c) {
  for(int i = up_left_c; i <= bot_right_c; i++) {
    for(int j = up_left_r; j <= bot_right_r; j++) {
      ip->data[i * ip->cols + j].r = 0;
      ip->data[i * ip->cols + j].b = 0;
      ip->data[i * ip->cols + j].g = 0;
    }
  }
  return ip;
}

/* Helper function for the blur operation 
 * Takes in a double value as its only parameter 
 * Returns the value squared
 */
double sq(double val) {
  return val * val;
}

/* Helper function for the blur operation 
 * Takes in the sigma value as its only parameter, and ensures the size is odd 
 * Creates a dynamically allocated matrix with the gaussian distribution values 
 * Returns the pointer to the matrix 
 */
double * nxnmatrix(double sigma) {
  int size = sigma * 10;
  if(size % 2 == 0) {
    size++;
  }
  int mid = size/2;
  double * matrix = malloc(sq(size) * sizeof(double));
  for(int i = 0; i < size; i++) {
    for(int j = 0; j < size; j++) {
      matrix[i * size + j] = (1.0 / (2.0 * PI * sq(sigma))) * exp((-(sq(abs(i-mid)) + sq(abs(j-mid)))) / (2 * sq(sigma)));
    }
  }
  return matrix;
}

/* Function takes in an image pointer and sigma value as the two parameters  
 * Declares necessary variables for looping through the image 
 * Calls the matrix function to create the matrix to be applied to the image 
 * Creates a new image to copy to, so that the one passed through isnt altered while copying 
 * Loops through every pixel in the image 
 * For each pixel, begins at the corner of the matrix and loops through until the value is a valid 
 * portion of the picture (enters immediately for non-edge cases) 
 * Then multiplies the points within the image that correspond to their points on the matrix by 
 * the value of the matrix, and keeps a running a running sum for each pixel 
 * Also keeps a running sum of the number of martrix values that are used, to divide by at the end 
 * After the entire matrix has been looped through, rgb values are assigned to that pixel
 * Returns the pointer to the altered image
 */
Image * blur(Image * ip, double sigma){
  int row = ip->rows;
  int col = ip->cols;
  int size = sigma * 10;
  if(size % 2 == 0) {
    size++;
  }
  int mid = size / 2;
  double rsum, bsum, gsum, matrix_sum;
  double * matrix = nxnmatrix(sigma);

  Image * img = (Image *)malloc(sizeof(Image));
  Pixel * p = (Pixel *)malloc(row * col * sizeof(Pixel));

  img->data = p;
  img->rows = row;
  img->cols = col;
  
  for(int i = 0; i < row; i++) {
    for(int j = 0; j < col; j++) {
      matrix_sum = 0.0;
      rsum = 0.0;
      bsum = 0.0;
      gsum = 0.0;
      
      for(int x = 0; x < size; x++) {
	for(int y = 0; y < size; y++) { 
	  if(x + (i - mid) >= 0 && x + (i - mid) < row && y + (j - mid) >= 0 && y + (j - mid) < col) {
	    rsum += (double)(ip->data[(x + (i - mid)) * col + (y + j - mid)].r) * matrix[x * size + y];
	    bsum += (double)(ip->data[(x + (i - mid)) * col + (y + j - mid)].b) * matrix[x * size + y];
	    gsum += (double)(ip->data[(x + (i - mid)) * col + (y + j - mid)].g) * matrix[x * size + y];
	    matrix_sum += matrix[x * size + y];
	  }
	}
      }

      unsigned char ravg = rsum/matrix_sum;
      unsigned char bavg = bsum/matrix_sum;
      unsigned char gavg = gsum/matrix_sum;

      img->data[i * col + j].r = ravg;
      img->data[i * col + j].b = bavg;
      img->data[i * col + j].g = gavg;
    }
  }

  for(int i = 0; i < ip->rows * ip->cols; i++) {
    ip->data[i] = img->data[i];
  }
  free(matrix);
  free(ip->data);
  ip->data = p;
  free(img);
  return ip;
}
