// executable.c
// 601.220, Fall 2018
// Claire Zou and Eduardo Aguila
// czou6 and eaguila6

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"
#include "imageManip.h"

int executable(int argc, char* argv[]) {
  /* Ensures that there are at least 3 command line arguments, the minimum 
   * requried for an executable, and two file names 
   */
  if(argc < 3) {  
    fprintf(stderr, "Failed to supply input and/or output filename\n");
    return 1;
  }

  /* Makes a char pointer to the second command-line argument and
   * checks that the input file ends in .ppm
   * Repeats same process for the output file 
   */
  char * ppm = argv[1];
  if(strcmp(ppm + (strlen(argv[1]) - 4), ".ppm") != 0) {
    fprintf(stderr, "Failed to supply valid input filename\n");
    return 1;
  }
  char * ppm1 = argv[2];
  if(strcmp(ppm1 + (strlen(argv[2]) - 4), ".ppm") != 0) {
    fprintf(stderr, "Failed to supply valid output filename\n");
    return 1;
  }
  
  /* Creates a file pointer and then opens the file for reading, to create 
   * a copy of the file being passed as the first command line argument 
   * Checks to ensure that the file was successfully opened 
   * Then proceeds ensure the file is a properly formatted PPM (first line P6)
   */
  FILE *fp = fopen(argv[1], "rb"); 
  if (!fp) {
    fprintf(stderr, "Input file could not be opened\n");
    return 2;
  }
  char str[10];
  int row = 0, col = 0, color_range = 0;
  char c;
  if((fscanf(fp, "%s", str) != 1) || (strcmp(str, "P6") != 0 )) {
    fprintf(stderr, "Specified input file is not a properly-formatted PPM file\n");
    return 3;
  }
  
  /* Following section reads through any comments that may exist in the file 
   * Begins by reading in character on new line after P6, and then advancing the 
   * file pointer until a new line is reached, indicating the end up the comment 
   */
  c = getc(fp); //reads in new line after P6                                                                  
  c = getc(fp); //read in comment signal (#), if it exists
  while (c == '#') {
    while (c != '\n') { //read through the entire line, until hits a \n char
      c = getc(fp);
    }
  }
  ungetc(c, fp); //unget back to the new-line character 

  /* Following section reads continues to read in file specifications 
   * First collects the column and row values and stores into ints, ensuring 
   * that both values are collected and are valid
   * Then reads the coloe range and ensures it is equal to 255
   */
  if ((fscanf(fp, "%d %d", &col, &row) != 2) || (row < 0) || (col < 0)) {
    fprintf(stderr, "Specified input file is not a properly-formatted PPM file\n");
    return 3;
  }
  if ((fscanf(fp, "%d", &color_range) != 1) || (color_range != 255)) { 
    fprintf(stderr, "Specified input file is not a properly-formatted PPM file\n");
    return 3;
  }

  /* Ensures at least 4 command line arguments are present, the minimum required 
   * for any successful call 
   */
  if (argc < 4) {
    fprintf(stderr, "Failed to supply operation name\n");
    return 4;
  }

  /* Creates a pointer to an image struct, and then calls the read function to 
   * create a copy of the image
   */
  Image * copy = read_ppm(fp, row, col); 
  fclose(fp);
  
  /* Following 'if' statements execute the function indicated by the 
   * 4th command line argument, indicating the desired operation on image 
   */
  if (strcmp(argv[3], "swap") == 0) {
    if (argc != 4) { //Only 4 arguments for this operation 
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    }
    swap(copy);

  } else if (strcmp(argv[3], "grayscale") == 0) {
    if (argc != 4) { //Only 4 arguments for this operation 
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    }
    grayscale(copy);

  } else if (strcmp(argv[3], "contrast") == 0) {
    if (argc != 5) { //Only 5 arguments for this operation 
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    }
    contrast(copy, atof(argv[4]));
  
  } else if (strcmp(argv[3], "zoom_in") == 0) {
    if (argc != 4) { //Only 4 arguments for this operation 
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    }
    zoom_in(copy);
  
  } else if (strcmp(argv[3], "zoom_out") == 0) {
    if (argc != 4) { //Only 4 arguments for this operation 
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    }
    zoom_out(copy);
    
  } else if (strcmp(argv[3], "occlude") == 0) {
    if (argc != 8) { //Only 8 arguments for this operation 
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    } //Ensures all parameters for occlude are valid 
    if (atoi(argv[4]) < 0 || atoi(argv[4]) > copy->rows
        || atoi(argv[5]) < 0 || atoi(argv[5]) > copy->cols
	|| atoi(argv[6]) < 0 || atoi(argv[6]) > copy->rows
	|| atoi(argv[7]) < 0 || atoi(argv[7]) > copy->cols
	|| atoi(argv[4]) > atoi(argv[6]) || atoi(argv[5]) > atoi(argv[7])) {
      fprintf(stderr, "Arguments for occlude operation were out of range for the given input image\n");
      return 6;
    }      
    occlude(copy, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
  
  } else if (strcmp(argv[3], "blur") == 0) {
    if (argc != 5) {
      fprintf(stderr, "Incorrect number of arguments supplied\n");
      return 5;
    }
    blur(copy, atof(argv[4]));
  
  } else {
    fprintf(stderr, "Operation name specified is invalid\n");
    return 4;
  }


  /* Creates a file pointer to a new file an opens for writing 
   * Writes the altered image into a new file 
   * Ensures that all of the pixels were successfully copied 
   */
  FILE *fp2 = fopen(argv[2], "wb");
  if (!fp2) {
    fprintf(stderr, "Output file could not be opened\n");
    return 7;
  }
  if (write_ppm(fp2, copy) != (copy->rows * copy->cols)) {
    fprintf(stderr, "Output file failed to write all pixels correctly\n");
    return 7;
  }
  fclose(fp2);
  return 0; 
}
