// executable.h
// 601.220, Fall 2018
// Claire Zou and Eduardo Aguila
// czou6 and eaguila6


#ifndef EXECUTABLE_H
#define EXECUTABLE_H

#include <stdio.h>

/* Parameters include the number of command-line arguments entered by the user 
 * as well as a pointer to a char array containing each of the arguments entered 
 * Functions as the main executable for the project.c file, by calling each of the 
 * specified operations from the command-line argument, and returning an int 
 * indicating whether the function was successful or not 
 * Returns 0 if successful, and non-zero for not successful
 */
int executable(int argc, char * argv[]);

#endif
